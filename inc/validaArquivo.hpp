#ifndef VALIDA_ARQUIVO_HPP
#define VALIDA_ARQUIVO_HPP

#include <iostream>
#include <fstream>

using namespace std;

class ValidaArquivo{

	public:
		string validarArquivo(ifstream * arquivo);

		void validaArquivoNovo(ofstream * arquivo);

		string validaNome(string nomeImagem);

		void ignoraComentario(ifstream * arquivo);

};

#endif
