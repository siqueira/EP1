#ifndef FILTRONEGATIVO_HPP
#define FILTRONEGATIVO_HPP

#include "imagem.hpp"
#include "filtro.hpp"
#include "cor.hpp"
#include <iostream>
#include <list>

using namespace std;

class FiltroNegativo : public Filtro{
	public:
		FiltroNegativo();
		list<Cor> aplicarFiltro(Imagem * imagem);
};
#endif
