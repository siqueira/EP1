#ifndef FILTROPRETOEBRANCO_HPP
#define FILTROPRETOEBRANCO_HPP

#include "imagem.hpp"
#include "filtro.hpp"
#include "cor.hpp"
#include <iostream>
#include <list>

using namespace std;

class FiltroPretoeBranco : public Filtro{
	public:
		list<Cor> aplicarFiltro(Imagem * imagem);
};

#endif
