#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP
#include <iostream>
#include "imagem.hpp"
#include "cor.hpp"
#include "filtro.hpp"
#include <list>

using namespace std;

class FiltroPolarizado : public Filtro{
	public:	
	FiltroPolarizado();
	list<Cor> aplicarFiltro(Imagem  * imagem);
};

#endif
