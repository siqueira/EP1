#ifndef FILTROMEDIA_HPP
#define FILTROMEDIA_HPP

#include <iostream>
#include "imagem.hpp"
#include "cor.hpp"
#include "filtro.hpp"
#include <list>

class FiltroMedia : public Filtro{
	private:
		int mask;
	public:
		FiltroMedia();
		FiltroMedia(int mask);
		void setMask(int mask);
		int getMask();
		list<Cor> aplicarFiltro(Imagem * imagem);
	
	private:		
		Cor ** getMatriz(int altura, int largura, list<Cor> coreslist);
		list<Cor> getList(Cor** matriz, int largura, list<Cor> coresList);

};
#endif
