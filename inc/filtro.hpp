#ifndef FILTRO_HPP
#define FILTRO_HPP

#include "cor.hpp"
#include "imagem.hpp"
#include <iostream>
#include <list>

using namespace std;

class Filtro {
	public:
		virtual list<Cor> aplicarFiltro(Imagem * imagem);		

};

#endif
