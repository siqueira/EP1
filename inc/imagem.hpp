#ifndef IMAGEM_HPP
#define IMAGEM_HPP

#include <iostream>
#include <string>
#include <list>
#include "validaArquivo.hpp"
#include "cor.hpp"
#include <fstream>

using namespace std;

class Imagem {
	private:
		string formato;
		int altura;
		int largura;
		string endereco;		
		unsigned char maxEscala;
		list<Cor> cores;
		int isOpcao(int opcao);

	public:			
		Imagem();
		Imagem(string formato, int altura, int largura, unsigned char maxEscala, list<Cor> cores);		
		void setFormato(string formato);
		string getFormato();
		int getAltura();
		void setAltura(int altura);
		int getLargura();
		void setLargura(int largura);
		void setMaxEscala(unsigned char maxEscala);
		unsigned char getMaxEscala();
		string getEndereco();
		void setEndereco(string endereco);
		list<Cor> getCores();
		void setCores(list<Cor> cores);
		int cabecalho();
		int getTamanhoCabecalho();
		list<Cor> lerCores(ifstream * arquivo);
		void lerImagem();
		void imprimeDados();
		void gravarImagem();
		string getDado(ifstream * arquivo);
};
#endif
