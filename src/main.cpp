#include <iostream>
#include <fstream>
#include <string>
#include "imagem.hpp"
#include "cor.hpp"
#include "filtro.hpp"
#include "filtroNegativo.hpp"
#include "filtroPretoeBranco.hpp"
#include "filtroMedia.hpp"
#include "filtroPolarizado.hpp"
#include <list>

using namespace std;

int isOpcao(int opcao, int menor, int maior){
	while(opcao < menor || opcao > maior){
		cout << "Opcao Inválida, digite um numero entre 1 e 4\n";
		cin >> opcao;	
	}
	return opcao;
}

int opcaoMascara(){
	int escolha;	
	cout << endl;
	cout << "ESCOLHA A MÁSCARA DESEJADA:\n" << endl;
	cout << endl;
	cout << " 1 - 3 x 3\n 2 - 5 x 5\n 3 - 7 x 7 \n" << endl;
	cout << endl;
	cout << "Digite a opção: ";
	cin >> escolha;	
	isOpcao(escolha,1,3);

	switch(escolha){
		case 1:
			return 3;
		case 2:
			return 5;
		default:
			return 7;
	}
}

Filtro * opcaoFiltro(){
	int escolha;	
	cout << endl;
	cout << "ESCOLHA O FILTRO DESEJADO:\n" << endl;
	cout << endl;
	cout << " 1 - Polarizado\n 2 - Negativo\n 3 - Preto e Branco\n 4 - Média\n " << endl;
	cout << endl;
	cout << "Digite a opção: ";
	cin >> escolha;	
	isOpcao(escolha,1,4);

	Filtro * filtro = new Filtro();

	switch(escolha){
		case 1 : 
			filtro = new FiltroPolarizado();
			return filtro;
		case 2 :
			filtro = new FiltroNegativo();
			return filtro;
		case 3 :
			filtro = new FiltroPretoeBranco();
			return filtro;
		case 4:
			filtro = new FiltroMedia(opcaoMascara());
			return filtro;
	}
	return filtro;
}

int main(){
	Imagem * imagem = new Imagem();
	imagem->lerImagem();
	imagem->imprimeDados();	
	Filtro * filtro = opcaoFiltro();  		
	imagem->setCores(filtro->aplicarFiltro(imagem));
	imagem->gravarImagem();
	return 0;
}
