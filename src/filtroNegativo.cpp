#include "filtroNegativo.hpp"

using namespace std;

FiltroNegativo::FiltroNegativo(){
}

list<Cor> FiltroNegativo::aplicarFiltro(Imagem * imagem){
	list<Cor> cores;

	for(Cor cor : imagem->getCores()){
		cor.setR(imagem->getMaxEscala()-cor.getR());
		cor.setG(imagem->getMaxEscala()-cor.getG());
		cor.setB(imagem->getMaxEscala()-cor.getB());
		cores.push_back(cor);
	}
	return cores;
}
