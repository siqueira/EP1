#include "imagem.hpp"
#include "cor.hpp"
#include <sstream>

using namespace std;

Imagem::Imagem(){

}

Imagem::Imagem(string formato, int altura, int largura, unsigned char maxEscala, list<Cor> cores){
	this->maxEscala = maxEscala;
	this->cores = cores;
	this->formato = formato;
	this->altura = altura;
	this->largura = largura;
}

string Imagem::getFormato(){
	return formato;
}

void Imagem::setFormato(string formato){
	this->formato = formato;
}

int Imagem::getAltura(){
	return altura;
}

void Imagem::setAltura(int altura){
	this->altura = altura;
}
		
int Imagem::getLargura(){
	return largura;
}

void Imagem::setLargura(int largura){
	this->largura = largura;
}

unsigned char Imagem::getMaxEscala(){
	return maxEscala;
}

void Imagem::setMaxEscala(unsigned char maxEscala){
	this->maxEscala = maxEscala;
}

string Imagem::getEndereco(){
	return endereco;
}

void Imagem::setEndereco(string endereco){
	this->endereco = endereco;
}

list<Cor> Imagem::getCores(){
	return cores;
}

void Imagem::setCores(list<Cor> cores){
	this->cores = cores;
}

void Imagem::lerImagem(){
	ifstream arquivo;
	ValidaArquivo valid;
	setEndereco(valid.validarArquivo(&arquivo));
	arquivo.seekg(cabecalho(),ios_base::cur);
	setCores(lerCores(&arquivo));
	arquivo.close();
}

int Imagem::cabecalho(){
	ifstream arquivo;
	arquivo.open(getEndereco().c_str());
	ValidaArquivo valid;
	valid.ignoraComentario(&arquivo);
	setFormato(getDado(&arquivo));
	valid.ignoraComentario(&arquivo);
	int altura;
	int largura;
	istringstream * iss = new istringstream();
	iss->str(getDado(&arquivo));
	(*iss) >> largura >> altura;
	setAltura(altura);
	setLargura(largura);
	valid.ignoraComentario(&arquivo);	
	int escala;
	iss = new istringstream();
	iss->str(getDado(&arquivo));
	(*iss) >> escala;
	setMaxEscala(escala);
	valid.ignoraComentario(&arquivo);
	int posicao = arquivo.tellg();
	arquivo.close();
	return posicao;
}

int Imagem::getTamanhoCabecalho(){
	ifstream arquivo;
	arquivo.open(getEndereco().c_str());
	ValidaArquivo valid;
	valid.ignoraComentario(&arquivo);
	getDado(&arquivo);
	valid.ignoraComentario(&arquivo);
	getDado(&arquivo);
	valid.ignoraComentario(&arquivo);
	getDado(&arquivo);
	valid.ignoraComentario(&arquivo);	
	int tam = arquivo.tellg();
	return tam - 1;
}

list<Cor> Imagem::lerCores(ifstream * arquivo){
	Cor cor;
	list<Cor> cores;
	while(!arquivo->eof()){
		unsigned char dado = arquivo->get();
		cor.setR(dado);
		dado = arquivo->get();
		cor.setG(dado);
		dado = arquivo->get();
		cor.setB(dado);
		cores.push_back(cor);
	}
	return cores;
}

void Imagem::imprimeDados(){
	cout << endl;
	cout << "CABEÇALHO :" << endl;
	cout << endl;
	cout << "Formato: " << getFormato() << "\t\t\t\n" << "Largura: "
 << getLargura() << "\t\t\t\n" << "Altura " << getAltura() << "\t\t\t\n" 
		<< "Escala Máxima: ";
		printf("%d", getMaxEscala());
	cout << endl;
	cout << endl;
	cout << endl;
}

void Imagem::gravarImagem(){
	ofstream arquivoImagem;
	ValidaArquivo valid;
	valid.validaArquivoNovo(&arquivoImagem);
	int tam = getTamanhoCabecalho();
	ifstream arquivo;
	arquivo.open(getEndereco().c_str());
	while(arquivo.tellg() <= tam)
		arquivoImagem.put((unsigned char) arquivo.get());
	arquivo.close();
	
	for(Cor cor : getCores()){
		arquivoImagem.put(cor.getR());
		arquivoImagem.put(cor.getG());
		arquivoImagem.put(cor.getB());
		
	}
	
	arquivoImagem.close();
	cout << endl;
	cout << "\t\t\tImagem Nova criada com sucesso" << endl;
	cout << endl;
}

string Imagem::getDado(ifstream * arquivo){
	unsigned char byte;
	string dado;
	while((byte=arquivo->get()) != '\n'){
		dado += byte;
	}
	return dado;
}	

