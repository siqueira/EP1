#include "filtroPretoeBranco.hpp"

list<Cor> FiltroPretoeBranco::aplicarFiltro(Imagem * imagem){
	list<Cor> cores;
	for(Cor cor: imagem->getCores()){
		int grayscale_valor = (0.299 * cor.getR()) + (0.587 * cor.getG()) + (0.144 * cor.getB());
		cor.setR(grayscale_valor);
		cor.setG(grayscale_valor);
		cor.setB(grayscale_valor);
		cores.push_back(cor);
	}
	return cores;
}
