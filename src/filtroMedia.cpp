#include "filtroMedia.hpp"

using namespace std;

FiltroMedia::FiltroMedia(){
	this->mask = 7;
}

FiltroMedia::FiltroMedia(int mask){
	this->mask = mask;
}

int FiltroMedia::getMask(){
	return mask;
}

void FiltroMedia::setMask(int mask){
	this->mask = mask;
}

list<Cor> FiltroMedia::aplicarFiltro(Imagem * imagem){
	
	int limite = getMask()/2;
	int divisao = getMask()*getMask();	
	int mediaR;
	int mediaG;
	int mediaB;
	int i;
	int j;
	int k;
	int l;

	Cor ** matriz = getMatriz(imagem->getAltura(), imagem->getLargura(), imagem->getCores());	
	
	for(i = limite; i <  imagem->getAltura() - limite; i++){
		for(j = limite; j < imagem->getLargura() - limite; j++){
			
			mediaR = 0;
			mediaG = 0;
			mediaB = 0;

			for(k = -limite; k <= limite;k++){
				for(l = -limite; l <= limite; l++){
					mediaR += matriz[k+i][l+j].getR();
					mediaG += matriz[k+i][l+j].getG();
					mediaB += matriz[k+i][l+j].getB();
				}	
			}
			
			mediaR /= divisao;
			mediaG /= divisao;
			mediaB /= divisao;
			
			matriz[i][j].setR(mediaR);
			matriz[i][j].setG(mediaG);
			matriz[i][j].setB(mediaB);
		}
	}

	list<Cor> cores = getList(matriz,imagem->getLargura(),imagem->getCores());

	return cores;
}

Cor ** FiltroMedia::getMatriz(int altura, int largura, list<Cor> coreslist){
	
	Cor ** matriz = new Cor*[altura];
	int i = 0;		
	int linha=0;
	int coluna=0;
	

	for(i=0; i < altura+1; ++i){
		matriz[i] = new Cor[largura];	
	}
		
	for(Cor cor : coreslist){

		matriz[linha][coluna] = cor;
		
		if(coluna == largura - 1){
			coluna = -1;
			++linha;		
		}

		++coluna;		
	}

	return matriz;	
}

list<Cor> FiltroMedia::getList(Cor** matriz, int largura, list<Cor> coresList){

	list<Cor> newCoresList;

	int linha = 0;
	int coluna = 0;

	for(Cor cor : coresList){
		
		cor = matriz[linha][coluna];		

		newCoresList.push_back(cor);
		
		if(coluna == largura - 1){
			coluna = -1;
			++linha;		
		}

		++coluna;		
	}

	return newCoresList;
}


















