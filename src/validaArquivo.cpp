#include <iostream>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <algorithm>
#include "validaArquivo.hpp"

using namespace std;

string ValidaArquivo::validarArquivo(ifstream * arquivo){
	string nomeImagem;
	
	cout << "Digite nome da imagem\n";
	cout << flush;
	cin >> nomeImagem;

	nomeImagem = validaNome(nomeImagem);

	string diretorio = "doc/" + nomeImagem + ".ppm";
	arquivo->open(diretorio.c_str());

	while(arquivo->fail()){
		cout << "Erro ao abrir o arquivo" << endl;
		cout << "tente novamente"<<endl;
		cout << flush;
		cin >> nomeImagem;

		nomeImagem = validaNome(nomeImagem);

		diretorio = "doc/" + nomeImagem + ".ppm";

		arquivo->open(diretorio.c_str());
	}
	
	return diretorio;

}

string ValidaArquivo::validaNome(string nomeImagem){

	string extensao;
	int size = nomeImagem.size() - 1;

	while(true){
		extensao += nomeImagem[size];
		if(nomeImagem[size] == '.' || size == 0)
			break;		
		--size;		
	}
	reverse (extensao.begin(),extensao.end());
	
	if(extensao == ".ppm" || extensao == ".PPM"){
		nomeImagem.erase(size, nomeImagem.size() - 1);
		return nomeImagem;	
	}	
	else if(extensao[0] == '.') {
		cout << endl;
		cout <<"Extensao invalida" << endl;
		exit(1);	
	}	
	return nomeImagem;
	
}		

void ValidaArquivo::validaArquivoNovo(ofstream * arquivo){
	
	string nome;
	cout << "Informe o nome da imagem nova: ";
	cout << flush;
	cin >> nome;
	nome = validaNome(nome);
	string diretorio = "doc/" + nome + ".ppm";
	arquivo->open(diretorio.c_str());

	if(arquivo->fail()){
		cout << "Erro ao criar o novo arquivo"<< endl;
		exit(0);	
	}
}

void ValidaArquivo::ignoraComentario(ifstream * arquivo){

	char * linha = new char[1];
	arquivo->read(linha,1);

	if(linha[0] != '#'){
		arquivo->seekg(-1,ios_base::cur);
		return;	
	}

	while(linha[0] == '#'){
		char * caractere = new char[1];
		while(caractere[0] != '\n'){
			arquivo->read(caractere,1);
		}
		arquivo->read(linha,1);
	}
	
	arquivo->seekg(-1,ios_base::cur);
}
	
