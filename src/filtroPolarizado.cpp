#include "filtroPolarizado.hpp"

FiltroPolarizado::FiltroPolarizado(){

}

list<Cor> FiltroPolarizado::aplicarFiltro(Imagem  * imagem){
	list<Cor> cores;

	for(Cor cor : imagem->getCores()){
		if(cor.getR() < imagem->getMaxEscala()/2){
	           	cor.setR(0);
		}else{
			cor.setR(imagem->getMaxEscala());
		}
		 if(cor.getB() < imagem->getMaxEscala()/2){
			cor.setB(0);
		}else{
			cor.setB(imagem->getMaxEscala());
		}
		if(cor.getG() < imagem->getMaxEscala()/2){
	        	cor.setG(0);
		}else{
	        	cor.setG(imagem->getMaxEscala());
		}
		cores.push_back(cor);
	}
	return cores;
}
